#GENERAL:
alias -s exe=mono

alias cls="clear"
alias chmod="chmod -c"
alias chown="chown -c"

alias du='du -sh'

#USER:
alias bat='upower -i $(upower -e | grep 'BAT') | grep -E "state|to\ full|percentage"'

#emacs
alias Esummon='emacs --daemon'
alias Ekill='emacsclient -e "(kill-emacs)"'
alias ec='emacsclient -nw'
alias e='emacsclient -c -a ""'
alias n='nano'

#modern editors
alias s='subl3'
alias g='gedit'
alias k='kate'

#because of zsh ^
alias calc='noglob _calc'

#DEVELOPPMENT:
alias genpatch='diff -Naur'

alias gl='git pull'
alias gb='git branch'
alias gd='git diff'
alias ggl='git pull;git submodule -q foreach git pull -q origin master'
alias gp='git push'
alias gsync='git fetch upstream master;git pull --rebase upstream master;git push -f;'
alias gmerge='git fetch upstream;git merge upstream/master;git push'
alias gclean='git clean -fxd'
alias gc='git checkout'
alias glog='git log `git log -1 --format=%H -- CHANGELOG*`..; cat CHANGELOG*'

#GENTOO
alias amaterasu='sharingan sync && emerge --sync && eix-update'
alias coffee='haskell-updater && emerge -vuDN --with-bdeps=y world @smart-live-rebuild && emerge @preserved-rebuild && emerge --depclean'
