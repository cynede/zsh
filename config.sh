ZDOTDIR=${ZDOTDIR:-${HOME}}
ZSHDDIR="${HOME}/.config/zsh.d"
HISTFILE="${ZDOTDIR}/.zsh_history"
HISTSIZE='10000'
SAVEHIST="${HISTSIZE}"
autoload colors && colors

termtitle() {
	case "$TERM" in
		rxvt*|xterm|konsole|tmux|emacs)
			case "$1" in
				precmd)
					print -Pn "\e]0;%n@%m: %~\a"
				;;
				preexec)
					zsh_cmd_title="$2"
					zsh_cmd_title="${zsh_cmd_title//\\/\\\\}"
					zsh_cmd_title="${zsh_cmd_title//\$/\\\\\$}"
					zsh_cmd_title="${zsh_cmd_title//\%/<percent>}"
					print -Pn "\e]0;${zsh_cmd_title} [%n@%m: %~]\a"
				;;
			esac
		;;
	esac
}

setopt extendedGlob
autoload -U zmv
autoload -U zargs
setopt promptsubst

# Completion.
autoload -Uz compinit
compinit
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate
zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $(( ($#PREFIX+$#SUFFIX)/3 )) numeric )'
zstyle ':completion:*:expand:*' tag-order all-expansions
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~''*?.old' '*?.pro'
zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=$color[darkblue]=$color[red]"
export LS_COLORS='no=00;35:fi=00;35:di=01;34:ln=04;36:pi=30:so=01;35:do=01;35:bd=30;01:cd=30;01:or=31;01:su=37:sg=30:tw=30:ow=34:st=37:ex=01;31:*.cmd=01;31:*.exe=01;31:*.com=01;31:*.btm=01;31:*.sh=01;31:*.run=01;31:*.tar=30:*.tgz=30:*.arj=30:*.taz=30:*.lzh=30:*.zip=30:*.z=30:*.Z=30:*.gz=30:*.bz2=30:*.deb=30:*.rpm=30:*.jar=30:*.rar=30:*.jpg=32:*.jpeg=32:*.gif=32:*.bmp=32:*.pbm=32:*.pgm=32:*.ppm=32:*.tga=32:*.xbm=32:*.xpm=32:*.tif=32:*.tiff=32:*.png=32:*.mov=34:*.mpg=34:*.mpeg=34:*.avi=34:*.fli=34:*.flv=34:*.3gp=34:*.mp4=34:*.divx=34:*.gl=32:*.dl=32:*.xcf=32:*.xwd=32:*.flac=35:*.mp3=35:*.mpc=35:*.ogg=35:*.wav=35:*.m3u=35:';
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

if [ "$USER" = 'root' ] && [ "$(cut -d ' ' -f 19 /proc/$$/stat)" -gt 0 ]; then
	renice -n 0 -p "$$" && echo "# Adjusted nice level for current shell to 0."
fi

PROMPT="%{$fg_bold[grey]%}λ %{$reset_color%}"
RPROMPT="%{$fg_bold[grey]%}%~/ %{$reset_color%}% %(?,%{$fg[green]%}Π%{$reset_color%},%{$fg[red]%}∅%{$reset_color%}"

# Ignore duplicate in history.
setopt hist_ignore_dups
setopt hist_ignore_space
setopt noflowcontrol

# Shell config.
umask 022
if ! [[ "${PATH}" =~ "^${HOME}/bin" ]]; then
	export PATH="${HOME}/bin:${PATH}"
fi
export EDITOR="nano"
export ALTERNATE_EDITOR="emacsclient"
export TMP="$HOME/tmp"
export TEMP="$TMP"
export TMPDIR="$TMP"
if [ ! -d "${TMP}" ]; then mkdir "${TMP}"; fi

autoload -U url-quote-magic
zle -N self-insert url-quote-magic

export LD_LIBRARY_PATH=/usr/local/lib64
#export LIBGL_DRIVERS_PATH=/usr/lib/dri

export QT_SELECT=qt5
