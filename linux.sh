if which colordiff > /dev/null 2>&1; then
    alias diff="colordiff -Nuar"
else
    alias diff="diff -Nuar"
fi

alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias ls='ls --color=auto --human-readable --group-directories-first --classify'

alias cdmount='mount -t iso9660 -o ro /dev/cdrom'
alias srmount='mount -t iso9660 -o ro /dev/sr0'
